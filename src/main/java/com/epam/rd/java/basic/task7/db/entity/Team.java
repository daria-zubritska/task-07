package com.epam.rd.java.basic.task7.db.entity;

public class Team {

	private int id;

	private String name;

	public Team(String name) {
		setName(name);
	}

	public Team() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static Team createTeam(String name) {
		return new Team(name);
	}
	
	public String toString() {
		return getName();
	}

	@Override
	public boolean equals(Object o) {
		if (o == this) {
            return true;
        }
        if (!(o instanceof Team)) {
            return false;
        }
        
        Team u = (Team)o;
        
        return this.name.equals(u.name);
	}

}
