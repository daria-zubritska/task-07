package com.epam.rd.java.basic.task7.db;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.epam.rd.java.basic.task7.db.entity.*;

public class DBManager {

	private static DBManager instance;

	private String getConnectionUrl() {
		Properties properties = new Properties();
		try (InputStream stream = Files.newInputStream(Paths.get("app.properties"))) {
			properties.load(stream);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return properties.getProperty("connection.url");
	}

	public static synchronized DBManager getInstance() {
		if (instance == null)
			instance = new DBManager();
		return instance;
	}

	public Connection getConnection() throws DBException {
		Connection connection = null;
		try {
			String connectionUrl = getConnectionUrl();
			connection = DriverManager.getConnection(connectionUrl);

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return connection;
	}

	private DBManager() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public List<User> findAllUsers() throws DBException {

		String req = "SELECT * FROM users";
		ArrayList<User> users = new ArrayList<>();

		try (Connection connection = getConnection();
				Statement statement = connection.createStatement();
				ResultSet resultSet = statement.executeQuery(req);) {

			while (resultSet.next()) {
				User u = new User();
				u.setId(resultSet.getInt("id"));
				u.setLogin(resultSet.getString("login"));
				users.add(u);
			}

			connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return users;
	}

	public boolean insertUser(User user) throws DBException {
        if (user == null) return false;
        String sql = "INSERT INTO users(login) VALUES(?)";
        try (
                Connection connection = getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        ) {
            preparedStatement.setString(1, user.getLogin());
            int affectedRows = preparedStatement.executeUpdate();
            if (affectedRows == 0) return false;

            try (ResultSet generatedKeysResult = preparedStatement.getGeneratedKeys()) {
                if (generatedKeysResult.next()) user.setId(generatedKeysResult.getInt(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
	}

	public boolean deleteUsers(User... users) throws DBException {
		String req = "DELETE FROM users WHERE id=?";
		int deletedCount = 0;

		try (Connection connection = getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(req);) {

			for (User u : users) {
				preparedStatement.setInt(1, u.getId());
				deletedCount += preparedStatement.executeUpdate();
			}

			connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return deletedCount != 0;
	}

	public User getUser(String login) throws DBException {
		String req = "SELECT * FROM users WHERE login=?";
		User u = new User();

		try (Connection connection = getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(req);) {

			preparedStatement.setString(1, login);
			ResultSet resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				u.setId(resultSet.getInt("id"));
				u.setLogin(resultSet.getString("login"));
			}

			connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return u;
	}

	public Team getTeam(String name) throws DBException {
		String req = "SELECT * FROM teams WHERE name=?";
		Team t = new Team();

		try (Connection connection = getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(req);) {

			preparedStatement.setString(1, name);
			ResultSet resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				t.setId(resultSet.getInt("id"));
				t.setName(resultSet.getString("name"));
			}

			connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return t;
	}

	public List<Team> findAllTeams() throws DBException {
		String req = "SELECT * FROM teams";
		List<Team> teams = new ArrayList<>();

		try (Connection connection = getConnection();
				Statement statement = connection.createStatement();
				ResultSet resultSet = statement.executeQuery(req);) {

			while (resultSet.next()) {
				Team t = new Team();
				t.setId(resultSet.getInt("id"));
				t.setName(resultSet.getString("name"));
				teams.add(t);
			}

			connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return teams;
	}

	public boolean insertTeam(Team team) throws DBException {
		if (team == null)
			return false;
		String sql = "INSERT INTO teams(name) VALUES(?)";
		try (Connection connection = getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(sql,
						Statement.RETURN_GENERATED_KEYS);) {

			preparedStatement.setString(1, team.getName());
			int affectedRows = preparedStatement.executeUpdate();

			if (affectedRows == 0)
				return false;

			try (ResultSet generatedKeysResult = preparedStatement.getGeneratedKeys()) {
				if (generatedKeysResult.next())
					team.setId(generatedKeysResult.getInt(1));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return true;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		String sql = "INSERT INTO users_teams values(?,?)";
        int affectedRows = 0;
        Connection connection = getConnection();


        try (
                PreparedStatement preparedStatement = connection.prepareStatement(sql);

        ) {
            connection.setAutoCommit(false);
            preparedStatement.setInt(1, user.getId());

            for (Team team : teams) {
                preparedStatement.setInt(2, team.getId());
                affectedRows += preparedStatement.executeUpdate();
            }
            connection.commit();

        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            throw new DBException(e.getMessage(),e);

        }
        return affectedRows != 0;
	}

	public List<Team> getUserTeams(User user) throws DBException {
		String sql =
                "SELECT * FROM teams \n" +
                        "LEFT JOIN users_teams ON users_teams.team_id = teams.id\n" +
                        "LEFT JOIN users ON users.id = users_teams.user_id\n" +
                        "WHERE users.id = ?";

        List<Team> userTeams = new ArrayList<>();
        try (
                Connection connection = getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(sql)
        ) {
            preparedStatement.setInt(1, user.getId());
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Team team = new Team();
                team.setId(resultSet.getInt("id"));
                team.setName(resultSet.getString("name"));
                userTeams.add(team);
            }


        } catch (SQLException e) {
            e.printStackTrace();
        }

        return userTeams;
	}

	public boolean deleteTeam(Team team) throws DBException {
		String sql = "DELETE FROM teams WHERE id = ?";
		int deletedCount = 0;
		try (Connection connection = getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
			
			preparedStatement.setInt(1, team.getId());
			deletedCount = preparedStatement.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return deletedCount != 0;
	}

	public boolean updateTeam(Team team) throws DBException {
		int affectedRows = 0;
		String req = "UPDATE teams SET name=? WHERE id=?";
		try (Connection connection = getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(req);) {

			preparedStatement.setString(1, team.getName());
			preparedStatement.setInt(2, team.getId());
			affectedRows = preparedStatement.executeUpdate();

			connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return affectedRows != 0;
	}

}
