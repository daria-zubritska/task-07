package com.epam.rd.java.basic.task7.db.entity;

public class User {

	private int id;

	private String login;

	public User(String login) {
		setLogin(login);
	}

	public User() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public static User createUser(String login) {
		return new User(login);
	}

	public String toString() {
		return getLogin();
	}

	@Override
	public boolean equals(Object o) {
		if (o == this) {
            return true;
        }
        if (!(o instanceof User)) {
            return false;
        }
        
        User u = (User)o;
        
        return this.login.equals(u.login);
	}

}
